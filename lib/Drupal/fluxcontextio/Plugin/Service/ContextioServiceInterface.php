<?php

/**
 * @file
 * Contains DropboxServiceInterface
 */

namespace Drupal\fluxcontextio\Plugin\Service;

use Drupal\fluxservice\Service\OAuthServiceInterface;

/**
 * Interface for Contextio services.
 */
interface ContextioServiceInterface extends OAuthServiceInterface {

}
