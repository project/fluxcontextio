<?php

/**
 * @file
 * Contains ContextioEntryInterface.
 */

namespace Drupal\fluxcontextio\Plugin\Entity;

use Drupal\fluxservice\Entity\RemoteEntityInterface;

/**
 * Provides a common interface for all Facebook objects.
 */
interface ContextioContactInterface extends RemoteEntityInterface {

}
